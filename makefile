all:
	@echo 'Nothing to make.'
	@echo 'If you want to run the ansible script against your inventory'
	@echo 'please use `make production`.'

prod: production

production:
	ansible-playbook -i inventory site.yaml
