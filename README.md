# Capture The Flag

This repository sets up the aucs server for a game of capture the flag where the objective is to discover the password for the next user and `su` into them.

It is written in [Ansible](https://www.ansible.com/).

To run it install Ansible and run the command:

```
ansible-playbook  -i inventory site.yaml
```

## Adding puzzles

Puzzles can be created in `roles/puzzles/tasks/` and added to the list of puzzles in `roles/puzzles/tasks/main.yaml`
