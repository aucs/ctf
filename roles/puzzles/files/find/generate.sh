#!/bin/bash
# Generate random files.
for h in {1..22}; do
	dd if=/dev/urandom of="file$h" bs=1M count=1;
done

# Create the file we want to find.
f="file11";
dd if=/dev/urandom of="$f" bs=8M count=1;
echo >> "$f";
echo "$1" >> "$f";

# Finally remove ourselves.
rm $0;
