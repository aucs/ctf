#!/usr/bin/env bash
# This file generates the git directory for the git puzzle.

[ -d "project" ] && rm -rf "project";

mkdir project;
cd project;

git init;

mkdir facts;
echo "$@" > password.txt;
echo "1pt chocolate cake" > secretcakerecipe.txt;
echo "3 scoops of sugar" >> secretcakerecipe.txt;
echo "no" > willtheworldbeeatenbyahugepiranhasomeday.txt;

echo "There is a lot of stuff in this world." > facts/fact;
echo "Cows produce milk, and sometimes methane." > facts/anotherfact;
git add .;
git commit -m "These facts the world needs to know.";

mkdir opinions;
echo "Blue is a boring colour." > opinions/opinion;
echo "White chocolate is better than dark chocolate." > opinions/anotheropinion;
echo "onion" > opinions/onion;
git add .;
git commit -m "These opinions the world does not need to know.";

mkdir src;
echo "Some actual coding." > src/README.md;
echo "intelligent=true;" > src/aiv2.cpp;
echo "if (not.at.goal()) {go.to.goal()}" > src/navigation_algorithm.cpp;
git add .;
git commit -m "These scripts will change the world.";

rm secretcakerecipe.txt;
rm password.txt;
rm willtheworldbeeatenbyahugepiranhasomeday.txt;
rm opinions/onion;
git add .;
git commit -m "Oops, removed something that wasn't supposed to be there. Also, I don't like onions.";

echo "onion" > opinions/onion;
git add .;
git commit -m "Everyone is entitled to their own onion.";

echo "This is a README file." > README.md;
git add .;
git commit -m "Added helpful readme.";

mkdir million_dollar_idea;
echo "make money" > million_dollar_idea/makefile;
git add .;
git commit -m "This is huge. Added million dollar idea.";

mv million_dollar_idea billion_dollar_idea;
git add .;
git commit -m "More like billion dollar idea.";

rm -rf facts opinions src;
git add .;
git commit -m "Cashed out; moving to Bahamas. See yas!"

rm $0;
