#!/usr/bin/env sh

# 98 + 12 = 2*X

SHA=$(echo $(((18 + (194759/14) + $X))) | sha256sum)

BASE16STRING=$(echo "$SHA" | md5sum | cut -b 12-20)

BASE16STRING=$(echo $BASE16STRING | awk '{print toupper($0)}')

echo "obase=10; ibase=16; $BASE16STRING" | bc
