#!/usr/bin/env bash
# This file generates the git directory for the git puzzle.

[ -d "project" ] && rm -rf "project";

mkdir project;
cd project;
git init;
mkdir src;
echo a* > src/astar.cpp;
echo "if(!intelligent){intelligent=true;}" > src/ai.cpp;
git add .;
git commit -m "Added basic framework for artificial intelligence.";

mkdir app;
touch app/page{1,2,3,4};
echo init > app/init;
git add app;
git commit -m "Added app to help program my homework.";

mkdir stuff;
touch stuff/stuff;
echo "This is a README file." > README.md;
git add .;
git commit -m "Added stuff.";
git checkout -b "readme-stuff" 2>&1;

echo "app.js" > app/script;
echo "make program" > src/makefile;
git add .;
git commit -m "Added more stuff. See my previous commits for more context.";

mkdir password;
touch password/password;
touch GIT_LOG.md;
git add .;
git commit -m "The password you seek is this commit's ID.";
git checkout -b "password" 2>&1;

rm -rf app;
echo "Another line of the README file." >> README.md;
git add .;
git commit -m "This is a happy little commit.";

mkdir app;
touch app/something;
git add .;
git commit -m "Oops, that was not supposed to happen. Also moved some stuff to other branches.";

git checkout master 2>&1;

rm $0;
