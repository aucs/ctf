#!/usr/bin/env bash
# Lots of cake.
# Made by ilmikko@ for AUCS
# Production of 2018

# Say that we won.
lolcat -a -p 20 -F 0.05 -d 42 won.txt;

sleep 3;

# Finish with cake.
while true; do
    lolcat -a -p 20 -F 0.1 -d 22 cake.txt;
done
