#!/bin/bash
# Generate random file structure.
for i in {1..10}; do
	d="directory$i";
	mkdir "$d";
	for h in {1..40}; do
		dd if=/dev/urandom of="$d/file$h" bs=1M count=1;
	done
done

# Create the file we want to find.
f="directory4/file22";
dd if=/dev/urandom of="$f" bs=8M count=1;
echo >> "$f";
echo "$1" >> "$f";

# Finally remove ourselves.
rm $0;
