#!/bin/bash
# Generate random file structure.
for i in {1..5}; do
	d="directory$i";
	mkdir "$d";
	for h in {1..70}; do
		dd if=/dev/urandom of="$d/file$h" bs=200K count=1;
	done
done

# Create the file we want to find.
f="directory2/file9";
echo > "$f";
for i in {1..122}; do
	echo $i | sha1sum >> "$f";
done
echo "key: $@" >> "$f";
for i in {1..155}; do
	echo $i | sha1sum >> "$f";
done

# Finally remove ourselves.
rm $0;
